package ru.korotkov.tm;

import ru.korotkov.tm.constant.TerminalConst;
import ru.korotkov.tm.controller.*;
import ru.korotkov.tm.entity.Project;
import ru.korotkov.tm.entity.Task;
import ru.korotkov.tm.enumerated.Role;
import ru.korotkov.tm.repository.ProjectRepository;
import ru.korotkov.tm.repository.TaskRepository;
import ru.korotkov.tm.repository.UserRepository;
import ru.korotkov.tm.service.ProjectService;
import ru.korotkov.tm.service.ProjectTaskService;
import ru.korotkov.tm.service.TaskService;
import ru.korotkov.tm.service.UserService;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    private static final SystemController systemController = new SystemController();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository);

    private final UserController userController = new UserController(userService);

    {
        Project project = projectService.create("1", "2");
        Task task = taskService.create("3", "4");
        task.setProjectId(project.getId());
        userService.create("TEST", "TEST");
        userService.create("ADMIN", "ADMIN", Role.ADMIN);
    }

    public static void main(final String[] args) {
        final Application app = new Application();
        systemController.displayWelcome();
        app.run();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String command;
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (!process(command)) {
                break;
            }
        }
    }

    public boolean process(final String line) {
        if (line == null || line.isEmpty()) {
            return true;
        }

        if (TerminalConst.CMD_EXIT.equals(line)) {
            return false;
        }

        processCommand(line);
        return true;
    }


    private void processCommand(final String line) {
        final String parts[] = line.split(TerminalConst.SPLIT);
        final String command = parts[0];
        final String[] arguments = Arrays.copyOfRange(parts, 1, parts.length);
        switch (command) {
            case TerminalConst.CMD_VERSION:
                systemController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                systemController.displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                systemController.displayHelp();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject(arguments);
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.listProject();
                break;
            case TerminalConst.PROJECT_VIEW:
                projectController.viewProject(arguments);
                break;
            case TerminalConst.PROJECT_REMOVE:
                projectController.displayProject(projectTaskController.removeProject(arguments));
                break;
            case TerminalConst.PROJECT_UPDATE:
                projectController.updateProject(arguments);
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask(arguments);
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.listTask();
                break;
            case TerminalConst.TASK_VIEW:
                taskController.viewTask(arguments);
                break;
            case TerminalConst.TASK_REMOVE:
                taskController.removeTask(arguments);
                break;
            case TerminalConst.TASK_UPDATE:
                taskController.updateTask(arguments);
                break;
            case TerminalConst.TASK_VIEW_BY_PROJECT:
                taskController.findTaskByProjectId(arguments);
                break;
            case TerminalConst.TASK_ADD_TO_PROJECT:
                taskController.displayTask(projectTaskController.addTaskToProject(arguments));
                break;
            case TerminalConst.TASK_REMOVE_FROM_PROJECT:
                taskController.removeTaskFromProject(arguments);
                break;
            case TerminalConst.USER_CREATE:
                userController.createUser(arguments);
                break;
            case TerminalConst.USER_CLEAR:
                userController.clearUser();
                break;
            case TerminalConst.USER_LIST:
                userController.listUser();
                break;
            case TerminalConst.USER_VIEW:
                userController.viewUser(arguments);
                break;
            case TerminalConst.USER_REMOVE:
                userController.removeUser(arguments);
                break;
            case TerminalConst.USER_UPDATE:
                userController.updateUser(arguments);
                break;
            case TerminalConst.USER_REGISTER:
                userController.register(arguments);
                break;
            default:
                systemController.displayStub(line);
                break;
        }
    }

}